<img src="https://cdn.amvstr.ml/amv.png" width=30% />

# AMVSTRM

Nuxt 3 self-hostable anime streaming website with amvstrm's API.  

Read more in our [Documentation](https://amvdocs.pages.dev/introduction) 

## Donation

__English__

We have been running our website with no ADs and also provide users with free anime too.

Please consider supporting us via :

[LiberaPay (GLOBAL)](https://en.liberapay.com/amvstrm/)

or If you are Cambodian Citizen :

[ABA (USD)](https://aslnk.ml/aba.nyt92)  
[ABA (KHR)](https://aslnk.ml/aba.nyt92.khr)

__Khmer__

យើង​បាន​ដំណើរការ​គេហទំព័រ​របស់​យើង​ដោយ​មិន​មាន​ការ​ផ្សាយ​ពាណិជ្ជកម្ម ហើយ​ក៏​ផ្តល់​ឱ្យ​អ្នក​ប្រើ​នូវ​ Anime ដោយ​ឥត​គិត​ថ្លៃ​ផង​ដែរ។
សូមអ្នកជួយគាំទ្រយើងខ្ញុំតាមរយៈ

[LiberaPay (GLOBAL)](https://en.liberapay.com/amvstrm/)

ឬ

[ABA (USD)](https://aslnk.ml/aba.nyt92)  
[ABA (KHR)](https://aslnk.ml/aba.nyt92.khr)

